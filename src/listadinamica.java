/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pc
 */
public class listadinamica<T> implements Iterable<T> {
    private nodo<T> first;
    private nodo<T> last;
    private int size;
    
    public listadinamica(){
        this.first = null;
        this.last = null;
        this.size = 0;
    }
    
    public boolean isEmpty(){//si la lista vacia
        return size==0;
    }
    
    public int sizeList(){//retornar el tamanio
        return size;
    }
    
    private nodo<T> getNode(int index){// RETORNA LO QUE ES UN NODO ESPECIFICO POR EL INDEX
        if(isEmpty() || (index<0 || index >=sizeList())){
            return null;
        }else if(index == 0){
            return first;
        }else if(index == sizeList() -1){
            return last;
        }else{
            nodo<T> searchNodo = first;
            int count = 0;
            while(count < index){
                count++;
                searchNodo = searchNodo.getNext();
            }
            return searchNodo;
        }
    }
    
    public T get(int index){
        if(isEmpty() || (index<0 || index >=sizeList())){
            return null;
        }else if(index == 0){
            return first.getValue();
        }else if(index == sizeList() -1){
            return last.getValue();
        }else{
            nodo<T> searchNodeValue = getNode(index);
            return searchNodeValue.getValue();
        }
    }
    
    public T getFirst(){
        if(isEmpty()){
            return null;
        }else{
            return first.getValue();
        }
    }
    
    public T getLast(){
        if(isEmpty()){
            return null;
        }else{
            return last.getValue();
        }
    }
    
    
    public T addFirst(T value){
        nodo<T> newvalue;
        if(isEmpty()){
            newvalue = new nodo<T>(value,null);
            first = newvalue;
            last = newvalue;
        }else{
            newvalue = new nodo<T>(value,first);
            first = newvalue;
        }
        size++;
        return first.getValue();
    }
    
    public T addLast(T value){
        nodo<T> newvalue;
        if(isEmpty()){
            return addFirst(value);
        }else{
            newvalue = new nodo<T>(value,null);
            last.setNext(newvalue);
            last = newvalue;
        }
        size++;
        return last.getValue();
    }
    
    public T add(T value,int index){
        if(index == 0){
            return addFirst(value);
        }else if(index == sizeList()){
            return addLast(value);
        }else if((index < 0 || index >= sizeList()-1)){
            return null;
        }else{
            nodo<T> nodo_prev = getNode(index-1);
            nodo<T> nodo_current = getNode(index);
            nodo<T> newvalue = new nodo<T>(value,nodo_current);
            nodo_prev.setNext(newvalue);
            size++;
            return getNode(index).getValue();
        }
    }
    
    @Override
    public Iterator<T> iterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
  
}
